// Fill out your copyright notice in the Description page of Project Settings.

#include "TestPawn.h"

#include "FollowingSpline.h"
#include "Components/SplineComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Net/UnrealNetwork.h"

// Sets default values
ATestPawn::ATestPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called every frame
void ATestPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	if(SplineReference)
	{
		ActualSplineLength = SplineReference->GetSplineComponent()->GetSplineLength();

		Current = FMath::FInterpConstantTo(Current, Target, DeltaTime, Speed);

		//If the movement has come to an end
		if(Current == Target)
		{
			switch (CurrentDirectionAlongSpline)
			{
				case ETimelineDirection::Forward: Target = 0.f; Current = 1.f; CurrentDirectionAlongSpline = ETimelineDirection::Backward;
				break;
				
				case ETimelineDirection::Backward: Target = 1.f; Current = 0.f; CurrentDirectionAlongSpline = ETimelineDirection::Forward;
				default: ;
			}
		}
		else
		{
			ActualClampedValue = FMath::GetMappedRangeValueClamped(FVector2D(0.f, 1.f), FVector2D(0.f, ActualSplineLength), Current);
			ActualNewLocation = SplineReference->GetSplineComponent()->GetLocationAtDistanceAlongSpline(ActualClampedValue, ESplineCoordinateSpace::World);
			SetActorLocation(ActualNewLocation);
		}
	}
}