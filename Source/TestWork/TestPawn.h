// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/TimelineComponent.h"
#include "GameFramework/Pawn.h"
#include "TestPawn.generated.h"

UCLASS()
class TESTWORK_API ATestPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ATestPawn();

protected:

	/** Pawn movement speed along the spline */
	UPROPERTY(EditAnywhere, Category = "Settings")
	float Speed;
	
	/** Reference to the spline along which the pawn will move */
	UPROPERTY(EditAnywhere, Category = "Settings")
	class AFollowingSpline* SplineReference;

	/** Current length spline */
	UPROPERTY()
	float ActualSplineLength;

	/** Current movement along the spline (0 back, 1 forward) */
	UPROPERTY()
	float Current;

	/** Target movement along the spline (0 back, 1 forward) */
	UPROPERTY()
	float Target;

	/** Spline current direction */
	UPROPERTY()
	TEnumAsByte<ETimelineDirection::Type> CurrentDirectionAlongSpline;

	UPROPERTY()
	float ActualClampedValue;

	/** The current location where the pawn should move */
	FVector ActualNewLocation;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	void SetSpeed(float NewSpeed) { Speed = NewSpeed; }
	
};
