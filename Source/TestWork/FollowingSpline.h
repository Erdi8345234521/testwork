// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FollowingSpline.generated.h"


UCLASS()
class TESTWORK_API AFollowingSpline : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFollowingSpline();

protected:
	
	UPROPERTY(VisibleAnywhere)
	class USceneComponent* SceneComponent;
	
	UPROPERTY(VisibleAnywhere)
	class USplineComponent* SplineComponent;
	
public:

	USplineComponent* GetSplineComponent() const { return  SplineComponent; }
	
};
